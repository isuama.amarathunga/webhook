FROM python:3.9.5-alpine3.13 as base
LABEL OWNER="Isuru Amarathunga"
WORKDIR /app
COPY dependencies.txt /app
RUN pip install --target=/app/dependencies -r ./dependencies.txt

#SET STATIC CONTENT ON TOP / VLAD Advise
FROM python:3.9.5-alpine3.13
ENV GROUP_ID=1000 USER_ID=1000
RUN addgroup -g $GROUP_ID author
RUN adduser -D -u $USER_ID -G author isuru -s /bin/sh
USER isuru
WORKDIR /app
COPY --from=base /app /app
COPY app.py /app
ENV PYTHONPATH="${PYTHONPATH}:/app/dependencies"
CMD ["python", "-u","app.py"]