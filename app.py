from flask import Flask
from flask import request
from flask import jsonify

import os
import logging
import datetime
import json

app = Flask(__name__)
#logger = logging.getLogger("Webhook")

@app.route('/', methods=['GET'])
def index():
    return jsonify(
        status="Healthy",
        message='Welcome to Webhook!'
    ),200

@app.route('/webhook', methods=['POST'])
def create():
    data = request.get_json(force=True)

    try:
        #get current date
        dt = datetime.datetime.now()

        #write to a file
        with open('/tmp/data.json', 'a', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)
            f.write('\n')

#        logger.info("received data at "+ dt + data)
        return  jsonify(status="Healthy", date=dt, message=data), 201
    except:
        return jsonify(status="Unhealthy", message='Error while processing request.')


if __name__ == '__main__':
    ENVIRONMENT_DEBUG = os.environ.get("APP_DEBUG", False)
    ENVIRONMENT_PORT = os.environ.get("APP_PORT", 5000)
    app.run(host='0.0.0.0', port=ENVIRONMENT_PORT, debug=ENVIRONMENT_DEBUG)